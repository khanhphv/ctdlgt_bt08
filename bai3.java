import java.util.Scanner;

/**
 * Created by K on 27/10/2015.
 */
public class bai3 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n;
        n=in.nextInt();
        int [] x=new int [n];
        for(int i=0;i<n;i++)
               x[i]=in.nextInt();
        int low=0;
        int high=x.length-1;
        quicksort(low,high,x);
        for(int i=0;i<n;i++)
            System.out.print(x[i]);
    }
    public static void quicksort( int low, int high, int x[])
    {
        if(high<=low) return;
        int k=low;
        int v=x[k];
        int i=low,gt=high;
        while(i<=gt)
        {
            if( x[i]<v)
            {
                int tmp=x[i];
                x[i]=v;
                v=tmp;
                i++;
                k++;
            }
            else
            {
                if(x[i]>v)
                {
                        int tmp=x[i];
                        x[i]=x[gt];
                        x[gt]=tmp;
                        gt--;
                }
                else
                    if(x[i]==x[k])
                    {
                        i++;
                    }
            }
        }
        quicksort(low,k-1,x);
        quicksort(gt+1,high,x);
    }
}
