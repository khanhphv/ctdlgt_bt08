import java.util.Scanner;

/**
 * Created by K on 29/10/2015.
 */
public class bai4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size;
        size = in.nextInt();
        int[] a = new int[size];
        for (int i = 0; i < size; i++)
            a[i] = in.nextInt();
        int lo = 0, hi = a.length - 1;
        int n = hi / 2;
        int value = search(a, lo, hi, n);
        System.out.println(value);
    }
    static void swap(int a[],int i,int j)
    {
        int tmp=a[i];
        a[i]=a[j];
        a[j]=tmp;
    }
     static int search(int[] a, int lo, int hi, int n) {
        int lt=lo,gt=hi,i=lo;
        int v=a[lo];
        while(i<=gt)
        {
            if(a[i]<v) swap(a,i++,lt++);
            else if(a[i]>v) swap(a,i,gt--);
            else i++;
        }
        if(n>gt)  return search  (a,gt+1,hi,n);
        else if(n<lt) return search(a,lo,lt-1,n);
        else return a[gt];
    }
}



